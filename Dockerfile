FROM condaforge/miniforge3:24.7.1-2
MAINTAINER Sebastian Canzler <sebastian.canzler@ufz.de>
LABEL authors="sebastian.canzler@ufz.de" \
    description="Docker image of ProteinPrompt - a tool to predict protein-protein interactions based on amino acid sequences."


#Try This to get a bash in order to run 'source activate modfinder'
RUN rm /bin/sh && ln -s /bin/bash /bin/sh

RUN conda update -n base conda
COPY ./data/proteinPrompt.yml /
RUN conda env update --name root -f /proteinPrompt.yml && \
    rm -rf /opt/conda/pkgs/*
ENV PATH /opt/conda/envs/proteinPrompt/bin:$PATH

# copy the proteinPrompt files
RUN mkdir /proteinPrompt/
COPY ./proteinPrompt/*py /proteinPrompt/
ENV PATH /proteinPrompt:$PATH

# get the random forest model from zenodo
RUN mkdir /model/
#COPY ./data/rf_human_750_inv.rev.pkl /model/random_forest.pkl
RUN wget https://zenodo.org/record/5708224/files/rf_human_750_inv.rev.pkl?download=1 -O /model/random_forest.pkl
COPY ./data/all.proteins.HUMAN.autocorrelation.pbz2 /model/all.proteins.HUMAN.autocorrelation.pbz2

# make proteinPrompt executable
ENTRYPOINT ["python", "/proteinPrompt/ProteinPrompt.py"]
#CMD ["-h"]
