import sys
from numpy import mean, std, array
from re import search
import bz2
import _pickle as cPickle

def read_csv_to_dict( filename: str, delim: str = '\t') -> dict:
    """
    Read a CSV file with a given delimiter to create a dictionary.

    Parameters
    ----------
    filename: str
      CSV file to be read

    delim: str
      Delimiter to split the entries.

    Returns
    -------
    data: dictionary, keys are the entries of the first column

    """
    data = {}

    with open( filename) as r:
        for line in r:
            c = line.strip().split( delim)
            data[c[0]] = array([float(x) for x in c[1:]])

    return data


def read_compressed_pickle( filename: str) -> object:

    """

    Read bz2-compressed pickle file.

    :param filename: File to be decompressed and read.
    :return: Data object being read.
    """

    data = bz2.BZ2File(filename, 'rb')
    data = cPickle.load(data)

    return data


def write_compressed_pickle( filename: str, data: object) -> None:

    """

    Write bz2-compressed pickle file.

    :param filename: File to be compressed and written - without ending.
    """
    with bz2.BZ2File(  filename + '.pbz2', 'w') as f:
        cPickle.dump(data, f)



def read_csv_file( filename, delim='\t', pos = "POS"):
    """
    Read a CSV file with a given delimiter to used the data for machine learning purposes.

    Parameters
    ----------
    filename: str
      File to be read

    delim: str
      Delimiter to split the entries.

    pos: str
      String for positive classification
      If pos is None, the first column is used as line identifier. There is no classification.

    Returns
    -------
    data: numpy.array
      Dataarray from the CSV file data

    targets: list
      List of Classifications given in the CSV file, POS will match 1, NEG will match 0

    """

    with open( filename, 'r') as csv_file:

        data = []
        targets = []

        for line in csv_file:
            l = line.strip().split( delim)
            cl = l.pop(0)
            data.append( l)

            if pos == "None":
                targets.append( cl)
                continue
            if cl == pos:
                targets.append( 1)
            else:
                targets.append( 0)



    return array( data, dtype=float), targets


def write_prediction_scores( outfile: str, scores: array, ppairs: list, threshold: float = 0.0):

    """
    Writes the predicted scores into a csv output file

    :param outfile: Name of output file.
    :param scores:  Numpy array of prediction scores
    :param ppairs:  List of protein pairs.
    :return: Nothing.
    """

    with open( outfile, 'w') as f:

        for i in range(len(ppairs)):
            if scores[i][1] > threshold:
                f.write("\t".join( ppairs[i]) + "\t")
                f.write("\t".join( [ str(x) for x in scores[i]]) + "\n")


def write_csv_file( content, filename, sep='\t' ):
    """
    Writes the given content into a csv file with a specific separator.

    Parameters
    ----------
    content: numpy.ndarray or numpy.matrix
      Content that should be written into a file.

    filename: str
      Filename of the csv file.

    sep: str
      The separator used. Default: '\t'

    """

    content = array( content)

    with open( filename, 'w') as f:
        for row in content:
            f.write( sep.join( str(x) for x in row) )
            f.write( "\n")




def read_scales( filehandles, aa ):
    """
    Read a list of scale-based descriptors and combine them into a list.

    Parameters
    ----------
    filehandles: list
      List of already opened filehandles, that contain the scale-based descriptors.

    aa: dict
      Dictionary that maps 3 letter to 1 letter amino acid abbreviations

    Returns
    -------
    scales: list
      List of dictionaries. Each appended dictionary describes one scale.

    """

    ## create an empty return list
    scales = []

    ## go through all given filehandles, which represent a scale-based descriptor
    for f in filehandles:

        current_scale = {}

        ## go through file and extract the values for each amino acid
        for line in f:
            l = line.split()
            current_scale[ aa[ l[0] ] ] = float( l[1] )

        ## calculate mean and standard deviation of the whole scale
        m = mean( current_scale.values() )
        s = std( current_scale.values() )

        ## add extra information into scale dict
        current_scale[ "0" ] =  0.0
        current_scale[ "mean" ] = m
        current_scale[ "sd" ] = s
        current_scale[ "name" ] = f.name

        ## append scale dict to return list
        scales.append( current_scale)

    return scales




def read_min_max_scale( scale, filename ):
    """
    Read the global extrema (minima, maxima) to a given scale.

    Parameters
    ----------
    scale: string
      Scale name as it is written in the filename.

    filename: string
      Textfile where global extrema of scale-based descriptors are written.

    Returns
    --------
    extrema: dict
      Dictionary with 'min' and 'max' values for the given scale.

    """

    extrema = {}

    ## search in the given file for the given scale
    with open( filename, 'r') as f:

        for line in f:
            l = line.strip().split()

            if line[:5] == 'wsize': continue

            ## save the global extrema in the return dict
            if l[0] == scale:
                extrema['min'] = float( l[1])
                extrema['max'] = float( l[3])

    ## print a notification that scale was not found in given file
    if 'min' not in extrema:
        sys.stderr.write( scale + "\n" )
        sys.stderr.write( "ERROR: scale not found in scales_min_max.txt!!" )
        sys.exit()


    return extrema




def read_cutoffs( filename, cut = 3):
    """
    Read the scale-based definitions of thresholds.
    Each feature matrix below such thresholds might be set to zero afterwards.
    cut = 3:   1% of true contacts are below this value
    cut = 4:   2% of true contacts are below this value
    cut = 5:   5% of true contacts are below this value

    Parameters
    ----------
    filename: string
      Input file that specifies the cutoffs for true contacts, based on different scales and different percentiles.

    cut: int
      Indirectly specify the percentile to be used.

    Returns
    -------
    cutoffs: dict
      Dictionary that maps the cutoff to each scale.

    """

    cutoffs = {}

    ## open filehandle and go through it
    with open( filename, 'r' ) as f:
        for line in f:

            if line[0] == "#" : continue

            ## save the scale name and the minimum value that shall be kept
            l = line.split()
            cutoffs[ l[0] ] = float( l[ cut ] )


    return cutoffs




def read_contact_scores( filename ):
    """
    Read a given input file containing statistical potentials of amino acid pairing probabilities

    Parameters
    ----------
    filename: string
      Input file name.

    Returns
    -------
    contact_score: dict
      Statistical potential for each amino acid pair to occur.

    """

    contact_score = {}

    with open( filename, 'r' ) as f:
        for line in f:

            l = line.split()

            ## save potential for both directions: "A-G" and "G-A"
            contact_score[ l[0] + l[1] ] = float( l[2])
            contact_score[ l[1] + l[0] ] = float( l[2])

    return contact_score




def read_min_max_contacts( filename, winsize = 1 ):
    """
    Read file that specifies the min and max of contact potentials.

    Parameters
    ----------
    filename: string
      Name of the file to read.

    Returns
    -------
    min_max: dict
      Dictionary containing the minimum and maximum of the contact potentials

    """

    min_max = {}

    ## open inputfile and go through it
    with open( filename, 'r') as f:
        for line in f:

            l = line.strip().split()
            if line[:5] == "wsize": continue

            ## get the correct extrema
            if int( l[0]) == winsize:
                min_max[ 'min' ] = float( l[1])
                min_max[ 'max' ] = float( l[3])

    ## print error message and exit if there are any troubles.
    if 'min' not in min_max:
        print( winsize)
        sys.stderr.write( "ERROR: scale not found in scales_min_max.txt!!" )
        sys.exit()



    return min_max



def read_sse( handle):
    """
    Read precited secondary structure information of antigens

    Parameters
    ----------
    handle: filehandle
      Already opened filehandle to read the structure information

    Returns
    -------
    structure: dict
      Dictionary with antigen name as key and a list of structure probabilities as value.

    """

    structure = {}

    for line in handle:
        l = line.strip().split()
        structure[ l.pop(0) ] = l

    return structure




def get_protein_mapping():
    """
    Read the all.proteins.mapping file and create a dictionary of all proteins and their cluster header.

    Returns
    -------
    mapping: dict
      Dictionary that links all proteins to their respective cluster header.

    """

    mapping = {}

    with open( "/project/data/project/protein_protein_interaction/dataset/all.proteins.mapping", 'r') as f:

        for line in f:
            l = line.strip().split()

            for prot in l:
                mapping[prot] = l[0]


    return mapping


def read_fasta_file( filename: str) -> dict:

    """

    Read in a fasta file.

    :param filename: Filename to read.
    :return: Dictionary, key: fastaheader, value: fastasequence
    """

    proteins = {}
    key = ""

    with open( filename, 'r') as ff:
        for line in ff:

            line = line.rstrip()
            if search(">", line):
                key = line[1:]
            else:
                proteins[key] = line

    return proteins

def write_feature_CSV_file( filename: str, features: dict, delim: str = "\t") -> None:

    """

    Write the content of the given dictionary as CSV file, where keys are written as the first column.

    :param filename: Output file name
    :param features: Content to be written into the file - Dictionary needed.
    :param delim: Delimeter to separate the columns
    """

    with open(filename, "w") as f:
        for key in features:
            f.write( key + delim)
            f.write(delim.join([str(x) for x in features[key]]) + "\n")
