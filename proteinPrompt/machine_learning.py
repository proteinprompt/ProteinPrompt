from sklearn.ensemble import RandomForestClassifier
from sklearn.ensemble import ExtraTreesClassifier
from sklearn import svm
from sklearn import neural_network as nnet
import sklearn.metrics as met
import joblib
from sklearn.calibration import calibration_curve
import numpy as np
import pickle
import gzip


def train_svm_radial( train, train_target):

    mod = svm.SVC( probability = True, random_state = 0)

    mod.fit( train, train_target)

    return mod



def train_neural_net( train, train_target):

    mod = nnet.MLPClassifier(solver='lbfgs', alpha=1e-5, hidden_layer_sizes=(4,), random_state=0)

    mod.fit( train, train_target)

    return mod

def train_extra_tree( train, train_target, n_estimators = 750):

    et = ExtraTreesClassifier(n_estimators=n_estimators, max_depth=None, min_samples_split=2, random_state=0,)
    et.fit( train, np.ravel(train_target))
    return et

def train_random_forest( train, train_target, njobs = 1, max_features = "sqrt", n_estimators = 1000):
    """
    Run a random forest machine learning to the given training data.

    Parameters
    ----------
    train: numpy.array
      Array containing the pure training data

    train_target: list
        Array with the class-ids for the training data.

    Returns
    -------
    rf: RandomForestClassifier
      RandomForestClassifier object trained on the basis of the given training data

    """

    rf = RandomForestClassifier(n_estimators=n_estimators, n_jobs = njobs, max_features= max_features, random_state=0)

    rf.fit(train, np.ravel(train_target))


    return rf



def predict_prob_dataset( model, test_data):
    """
    Classfication of the testing data with a given model.

    Parameters
    ----------
    model: model object
      A trained sklearn object.

    test_data: numpy.ndarray
      Actual data to classify.

    Returns
    -------
    prediction: numpy.adarray
      Array with classification probabilities, dimension ( nr_data, nr_classes )

    """

    prediction = model.predict_proba( test_data)

    return prediction




def predict_dataset( model, test_data):
    """
    Classfication of the testing data with a given model.

    Parameters
    ----------
    model: model object
      A trained sklearn object.

    test_data: numpy.ndarray
      Actual data to classify.

    Returns
    -------
    prediction: numpy.adarray
      Array with classification probabilities, dimension ( nr_data, nr_classes )

    """

    prediction = model.predict( test_data)

    return prediction




def calc_ROC( probabilities, reference):
    """
    Calculate the receiver operating characteristics of given test probabilities.

    Parameters
    ----------
    probabilities: numpy.array
      Array of predicted class probabilities, dimension ( nr_data, nr_classes )

    reference: list
      List of true Class-ids of the given data

    Returns
    -------
    roc_auc: float
      Area under the receiver operating characteristics curve.

    fpr:
    tpr:
      False and True positives.

    """

    fpr, tpr, _threshold = met.roc_curve( reference, probabilities[ :, 1] )
    roc_auc = met.auc(fpr, tpr)


    return roc_auc, fpr, tpr



def save_model( model, filename):
    """
    Save a given machine learning model to a file.

    Parameters
    ----------
    model: object
      Trained machine learning object thah should be saved.

    filename: str
      Filename of the saved model.

    """

    joblib.dump(model, filename, compress = 1)



def save_zipped_model( model, filename):
    """
        Save a given machine learning model to a file.

        Parameters
        ----------
        model: object
        Trained machine learning object thah should be saved.

        filename: str
        Filename of the saved model.

        """

    #    joblib.dump(model, filename, compress = 1)
    f = gzip.open( filename, 'wb')
    pickle.dump(model, f)
    f.close()



def load_model( filename):
    """
    Load a machine learning object from file.

    Parameters
    ----------
    filename: str
      Filename of the machine learning object that should be read.

    Returns
    -------
    model: object
      Loaded machine learning object.
    """

    model = joblib.load( filename)

    return model


def load_zipped_model( filename):
    """
        Load a machine learning object from file.

        Parameters
        ----------
        filename: str
        Filename of the machine learning object that should be read.

        Returns
        -------
        model: object
        Loaded machine learning object.
        """

    f = gzip.open( filename, 'rb')
    model = pickle.load( f)
    f.close()

    return model




def get_calibration_curve( test_target, prob_pos, bins = 10):
    """
    Calculate the calibration curve of a model, based on the probabilities of positive testing data.

    Parameters
    ----------
    test_target: list
      List of true testing data classification.

    prob_pos: list
      List of predicted testing data probabilities.

    bins: int
      Integer specifying the number of bins in the final curve.

    Returns
    -------
    fraction_of_positives: list
      The true fractions of positives in each bin.

    mean_predicted_value: list
      The predicted probability in each bin.


    """

    fraction_of_positives, mean_predicted_value = calibration_curve( test_target, prob_pos, n_bins = bins)

    return fraction_of_positives, mean_predicted_value
