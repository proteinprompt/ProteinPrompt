import numpy as np

def Scale2Profile( seq, scale):
    """
    Transform a amino acid string into a scale-based profile

    Parameters
    ----------
    seq: string
      Original amino acid sequence.

    scale: dict
      Dict that maps each amino acid to its respective scale-based value

    Returns
    -------
    vec: list
      Scale-based profile.

    """

    vec = [ scale[x] for x in seq ]

    return vec




def get_empty_array( length, values = 0.0 ):
    """
    Create an empty numpy.array element of a given length

    Parameters
    ----------
    length: int
      Length of the array.

    Returns
    -------
    array: numpy.array
      Array element of given length filled with 0.0, or given values.

    """

    array = np.array( [ values] * length )

    return array




def Contacts( seq1, seq2, contact_score ):
    """
    Calculate the statistical potential of two sequences to bind.
    Both sequences have to be of equal size, otherwise
    seq2 will only be processed to the same length of seq1.

    Parameters
    ----------
    seq1: list
      List of amino acids in sequence 1, can be of length 1.

    seq2: list
      List of amino acids in sequence 2, can be of length 1.

    contact_score: dict
      Dictionary with the pairwise pairing potentials.

    Returns
    -------
    contacts_pot: float
      The Sum of the contacts vector, adding all pairwise pairing potentials.

    """

    width = len( seq1)

    if width == 1:
        return contact_score[ seq1[0] + seq2[0] ]

    contacts = get_empty_array( width)

    ## go through both sequences and save the pairing potentials
    for i in range( width):

        ## get the dictionary key for the current amino acid pair
        ids = seq1[i]+seq2[i]

        ## save the potential in the vector
        if ids in contact_score:
            contacts[i] = float( contact_score[ ids ])
        else:
            contacts[i] = 1001.0

    ## calculate the overall binding potential of the given sequences
    contacts_pot = contacts.sum()


    return contacts_pot


def calcAutoCorrelation( seq, lag ):

    """
    Calculates the autocorrelation of a given sequence for pairs with a given lag distance.

    Parameters
    ----------
    seq: list
      List encoding the amino acid chain with a specific feature vector/

    lag: int
      Integer that specifies the distance between two amino acid pairs within seq.

    Returns
    -------
    correlation: float
      The correlation value.

    """

#    print seq[1:10]
    c_sum = 0.0
    mean = np.mean( seq )
    stddev = np.std( seq )
    length = len(seq) - lag

    for j in range( 0, length ):
        c_sum += ( seq[j] - mean ) * ( seq[j+lag] - mean )
#    print c_sum

    return ( c_sum / length ) / ( stddev * stddev )