import logging
import utils
import options
from typing import Tuple
from file_functions import read_csv_to_dict, write_prediction_scores
from machine_learning import load_model
from sklearn.ensemble import RandomForestClassifier
from numpy import array, concatenate, vstack

def construct_prediction_matrix( fdata: dict, ppFile: str) -> Tuple[array, list]:

    """

    Read the input protein pairs and construct the prediction matrix of feature vectors.

    :param fdata: Dictionary of feature vectors.
    :param ppFile: CSV file containing protein pairs.
    :return: Tuple. Prediction matrix and list of protein pairs
    """

    predict = []
    proteinPairs = []

    with open( ppFile, 'r') as f:
        for line in f:
            line = line.strip()
            (prot1, prot2) = line.split( "\t")
            proteinPairs.append( [prot1, prot2])
            predict.append(concatenate((fdata[prot1], fdata[prot2])))

    return vstack(predict), proteinPairs


def run(opts: options.PredictOptions) -> None:

    """

    The main function for predicting PPIs from given protein pairs.

    :param opts:
    :return: Nothing
    """

    logging.info("Predicting protein pairs.")

    # in case a fasta file was given, run autocorrelation to translate aa into feature vectors
    if opts.inputFile is not None and opts.inputCSV is None:
        logging.info("Translating protein fasta sequences into feature vectors.")
        opts.inputCSV = utils.featurize_fasta_to_file( opts.inputFile)

    # read feature vector csv file
    logging.info("Read protein feature vectors.")
    fdata = read_csv_to_dict( opts.inputCSV)

    # construct prediction matrix from given protein pairs
    logging.info("Construct prediction matrix.")
    predict, proteinPairs = construct_prediction_matrix( fdata, opts.proteinPairs)

    # predict the PPI scores
    if opts.modelType == "RF":
        logging.info("Predict the given protein pairs with random forests.")
        model = load_model( opts.modelRF)
        scores = model.predict_proba( predict)

        logging.info("Write prediction scores.")
        if opts.outputFile is not None:
            outfile = opts.outputDir + "/" + opts.outputFile
            write_prediction_scores( outfile, scores=scores, ppairs=proteinPairs)
        else:
            utils.print_prediction_scores( scores=scores, ppairs=proteinPairs)

    elif opts.modelType == "NN":
        logging.info("Predict the given protein pairs with neural networks.")

    elif opts.modelType == "HYBRID":
        logging.info("Predict the given protein pairs with a combined model.")

