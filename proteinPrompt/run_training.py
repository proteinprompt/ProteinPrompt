# This is a sample Python script.
import sys
import re
import file_functions as ff
import machine_learning as ml
import sklearn.metrics as met
import numpy as np
from timeit import default_timer as timer
import argparse as P
from os import path
from typing import Tuple

parse = P.ArgumentParser( description = "Train model and evaluate specific test sets from Marcotte lab. ")

parse.add_argument( '-t', '--train', type = str, required = True, metavar = 'FILE', help = 'Input proteins pairs that should be used for training.' )
parse.add_argument( '-d', '--db',    type = str, required = True, metavar = 'FILE', help = 'Pre-processed protein database (csv format)' )

args = parse.parse_args()

def read_training_file( file: str) -> np.array:

    train_list = []
    labels = []

    with open( args.train, 'r') as f:
        for line in f:
            line = line.strip()
            (label, prot1, prot2) = line.split( "\t")
            if( label == "Positive"):
                labels.extend([1, 1, 1, 1])
            else:
                labels.extend([0, 0, 0, 0])
            train_list.append(np.concatenate( (db[ prot1], db[ prot2])))
            train_list.append(np.concatenate( (db[ prot2], db[ prot1])))
            train_list.append(np.concatenate( (db[ prot1], db[ prot2 + "_inv"])))
            train_list.append(np.concatenate( (db[ prot2], db[ prot1 + "_inv"])))

    return np.vstack( train_list), labels


def evaluate_model( model, db: dict, file: str) -> Tuple[ float, float]:

    a = []
    b = []
    labels = []

    with open( file) as r:
        for line in r:
            c = line.strip().split('\t')
            a.append(db[c[1]])
            b.append(db[c[2]])
            if c[0] == 'Positive':
                labels.append(1)
            else:
                labels.append(0)

    a = np.array(a)
    b = np.array(b)
    m = np.concatenate((a, b), axis=1)
    p = model.predict_proba(m)

    fpr, tpr, _threshold = met.roc_curve(labels, p[:, 1])
    auroc = met.auc(fpr, tpr)

    precision, recall, _threshold = met.precision_recall_curve( labels, p[:, 1])
    auprc = met.auc( recall, precision)

    return auroc, auprc

# Press the green button in the gutter to run the script.
if __name__ == '__main__':

    model_file = re.sub( ".txt", "_rev_inv_2000.pkl", args.train)

    ## Step 1 - read database
    sys.stderr.write( "Read protein database.\n")
    db = ff.read_csv_to_dict( args.db, delim="\t")

    if( path.isfile( model_file)):

        sys.stderr.write( "Load random forest model.\n")
        model = ml.load_model( model_file)

    else:

        ## Step 2 - read training pairs
        sys.stderr.write( "Read training file.\n")
        train, labels = read_training_file( args.train)

        ## Step 3 - train RF model

        sys.stderr.write( "Train random forest model.\n")
        start = timer()
        model = ml.train_random_forest(train=train, train_target=labels, n_estimators=2000, njobs=5)
        #model = ml.train_extra_tree( train=train, train_target=labels)
        ml.save_model(model, model_file)
        end = timer()

        sys.stderr.write( "".join( ["Training took: ", str( end - start), " seconds.\n"]))

    ## Step 4 - evaluate model
    sys.stderr.write( "Evaluate random forest model.\n")

    ## In case CV training was used - we could only test against CV
    if( re.search( "CV", args.train)):
        test = re.sub("_train_", "_test_", args.train)
        m = re.search( "_C[V123]_(\d+).txt", test)
        auroc, auprc = evaluate_model( model, db, test)
        print( "CV\t%2.2f\t%2.2f\t%s" % (auroc, auprc, m.group(1)))

    ## In case non-CV training was used we could test against C1, C2, C3
    else:
        ## Test C1
        test = re.sub("_train_", "_test_C1_", args.train)
        m = re.search("_C[V123]_(\d+).txt", test)
        auroc, auprc = evaluate_model( model, db, test)
        print( "C1\t%2.2f\t%2.2f\t%s" % (auroc, auprc, m.group(1)))

        ## Test C2
        test = re.sub("_train_", "_test_C2_", args.train)
        m = re.search("_C[V123]_(\d+).txt", test)
        auroc, auprc = evaluate_model( model, db, test)
        print( "C2\t%2.2f\t%2.2f\t%s" % (auroc, auprc, m.group(1)))

        ## Test C3
        test = re.sub("_train_", "_test_C3_", args.train)
        m = re.search("_C[V123]_(\d+).txt", test)
        auroc, auprc = evaluate_model( model, db, test)
        print( "C3\t%2.2f\t%2.2f\t%s" % (auroc, auprc, m.group(1)))
