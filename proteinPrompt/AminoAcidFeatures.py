#!/usr/bin/python

import numpy
from collections import OrderedDict

def getTransformation( seq, trans ):

    trans_norm = normalize(trans)

    return [ trans_norm[aa] for aa in seq ]


def normalize( tr ):

    norm = {}
    mean = numpy.mean( list( tr.values() ))
    sd = numpy.std( list( tr.values() ))

    for key in tr:
        norm[key] = ( tr[key] - mean ) / sd

# same result
#    return {key:( tr[key] - mean ) / sd for key in tr }

    return norm

def getSignature( seq, sig_length ):

    amino_acids = ['A', 'C', 'D', 'E', 'F', 'G', 'H', 'I', 'K', 'L', 'M', 'N', 'P', 'Q', 'R', 'S', 'T', 'V', 'W', 'Y']

    signature = OrderedDict()

    ## initialize dict
    for pos1 in amino_acids:
        for pos2 in amino_acids:
            for pos3 in amino_acids:
                key = pos1 + pos2 + pos3
                signature[key] = 0.0

    for i in range(0, len(seq)-2):
        signature[seq[i:i+3]] += 1

    return [signature[key] for key in signature]



def getHydrophobicity( seq ):

    hydrophobicity = {
        'A' : 0.62,
        'C' : 0.29,
		'D' : -0.9,
		'E' : -0.74,
		'F' : 1.19,
		'G' : 0.48,
		'H' : -0.4,
		'I' : 1.38,
		'K' : -1.5,
		'L' : 1.06,
		'M' : 0.64,
		'N' : -0.78,
		'P' : 0.12,
		'Q' : -0.85,
		'R' : -2.53,
		'S' : -0.18,
		'T' : -0.05,
		'V' : 1.08,
		'W' : 0.81,
		'Y' : 0.26,
    }

    return getTransformation( seq, hydrophobicity )

def getHydrophilicity( seq ):

    hydrophilicity = {
        'A' : -0.5,
		'C' : -1,
		'D' : 3,
		'E' : 3,
		'F' : -2.5,
		'G' : 0,
		'H' : -0.5,
		'I' : -1.8,
		'K' : 3,
		'L' : -1.8,
		'M' : -1.3,
		'N' : 2,
		'P' : 0,
		'Q' : 0.2,
		'R' : 3,
		'S' : 0.3,
		'T' : -0.4,
		'V' : -1.5,
		'W' : -3.4,
		'Y' : -2.3,
    }

    return getTransformation( seq, hydrophilicity )


def getVolumeOfSideChains( seq ):

    volume = {
        'A' : 27.5,
		'C' : 44.6,
		'D' : 40,
		'E' : 62,
		'F' : 115.5,
		'G' : 0,
		'H' : 79,
		'I' : 93.5,
		'K' : 100,
		'L' : 93.5,
		'M' : 94.1,
		'N' : 58.7,
		'P' : 41.9,
		'Q' : 80.7,
		'R' : 105,
		'S' : 29.3,
		'T' : 51.3,
		'V' : 71.5,
		'W' : 145.5,
		'Y' : 117.3,
    }

    return getTransformation( seq, volume )


def getPolarity( seq ):

    polarity = {
		'A' : 8.1,
		'C' : 5.5,
		'D' : 13,
		'E' : 12.3,
		'F' : 5.2,
		'G' : 9,
		'H' : 10.4,
		'I' : 5.2,
		'K' : 11.3,
		'L' : 4.9,
		'M' : 5.7,
		'N' : 11.6,
		'P' : 8,
		'Q' : 10.5,
		'R' : 10.5,
		'S' : 9.2,
		'T' : 8.6,
		'V' : 5.9,
		'W' : 5.4,
		'Y' : 6.2,
    }

    return getTransformation( seq, polarity )


def getPolarizability( seq ):

    polarizability = {
		'A' : 0.046,
		'C' : 0.128,
		'D' : 0.105,
		'E' : 0.151,
		'F' : 0.29,
		'G' : 0,
		'H' : 0.23,
		'I' : 0.186,
		'K' : 0.219,
		'L' : 0.186,
		'M' : 0.221,
		'N' : 0.134,
		'P' : 0.131,
		'Q' : 0.18,
		'R' : 0.291,
		'S' : 0.062,
		'T' : 0.108,
		'V' : 0.14,
		'W' : 0.409,
		'Y' : 0.298,
    }

    return getTransformation( seq, polarizability )


def getSurfaceArea( seq ):

    sasa = {
		'A' : 1.181,
		'C' : 1.461,
		'D' : 1.587,
		'E' : 1.862,
		'F' : 2.228,
		'G' : 0.881,
		'H' : 2.025,
		'I' : 1.81,
		'K' : 2.258,
		'L' : 1.931,
		'M' : 2.034,
		'N' : 1.655,
		'P' : 1.468,
		'Q' : 1.932,
		'R' : 2.56,
		'S' : 1.298,
		'T' : 1.525,
		'V' : 1.645,
		'W' : 2.663,
		'Y' : 2.368,
    }

    return getTransformation( seq, sasa )


def getChargeOfSideChains( seq ):

    charge = {
		'A' : 0.007187,
		'C' : -0.03661,
		'D' : -0.02382,
		'E' : 0.006802,
		'F' : 0.037552,
		'G' : 0.179052,
		'H' : -0.01069,
		'I' : 0.021631,
		'K' : 0.017708,
		'L' : 0.051672,
		'M' : 0.002683,
		'N' : 0.005392,
		'P' : 0.239531,
		'Q' : 0.049211,
		'R' : 0.043587,
		'S' : 0.004627,
		'T' : 0.003352,
		'V' : 0.057004,
		'W' : 0.037977,
		'Y' : 0.023599,
    }

    return getTransformation( seq, charge )


def getTransmembraneTendency( seq ):

    membrane = {

'A' : 0.380,
'R' : -2.570,
'N' : -1.620,
'D' : -3.270,
'C' : -0.300,
'Q' : -1.840,
'E' : -2.900,
'G' : -0.190,
'H' : -1.440,
'I' : 1.970,
'L' : 1.820,
'K' : -3.460,
'M' : 1.400,
'F' : 1.980,
'P' : -1.440,
'S' : -0.530,
'T' : -0.320,
'W' : 1.530,
'Y' : 0.490,
'V' : 1.460,
    }

    return getTransformation( seq, membrane )


def getRefractivity( seq ):

    refractivity = {
'A' : 4.340,
'R' : 26.660,
'N' : 13.280,
'D' : 12.000,
'C' : 35.770,
'Q' : 17.560,
'E' : 17.260,
'G' : 0.000,
'H' : 21.810,
'I' : 19.060,
'L' : 18.780,
'K' : 21.290,
'M' : 21.640,
'F' : 29.400,
'P' : 10.930,
'S' : 6.350,
'T' : 11.010,
'W' : 42.530,
'Y' : 31.530,
'V' : 13.920,
        }

    return getTransformation(seq, refractivity )


def getAverageFlexibility( seq ):

    flexibility = {

'A' : 0.360,
'R' : 0.530,
'N' : 0.460,
'D' : 0.510,
'C' : 0.350,
'Q' : 0.490,
'E' : 0.500,
'G' : 0.540,
'H' : 0.320,
'I' : 0.460,
'L' : 0.370,
'K' : 0.470,
'M' : 0.300,
'F' : 0.310,
'P' : 0.510,
'S' : 0.510,
'T' : 0.440,
'W' : 0.310,
'Y' : 0.420,
'V' : 0.390,
    }

    return getTransformation( seq, flexibility )


def getBulkiness( seq ):

    bulkiness = {
'A' : 11.500,
'R' : 14.280,
'N' : 12.820,
'D' : 11.680,
'C' : 13.460,
'Q' : 14.450,
'E' : 13.570,
'G' : 3.400,
'H' : 13.690,
'I' : 21.400,
'L' : 21.400,
'K' : 15.710,
'M' : 16.250,
'F' : 19.800,
'P' : 17.430,
'S' : 9.470,
'T' : 15.770,
'W' : 21.670,
'Y' : 18.030,
'V' : 21.570,
    }

    return getTransformation( seq, bulkiness )


def getBuriedResidues( seq ):

    buriedResidues = {

'A' : 11.200,
'R' :  0.500,
'N' :  2.900,
'D' :  2.900,
'C' :  4.100,
'Q' :  1.600,
'E' :  1.800,
'G' : 11.800,
'H' :  2.000,
'I' :  8.600,
'L' : 11.700,
'K' :  0.500,
'M' :  1.900,
'F' :  5.100,
'P' :  2.700,
'S' :  8.000,
'T' :  4.900,
'W' :  2.200,
'Y' :  2.600,
'V' : 12.900,
    }

    return getTransformation( seq, buriedResidues )


def getAccessibleResidues( seq ):

    accessibleResidues = {

		'A' : 6.600,
		'R' : 4.500,
		'N' : 6.700,
		'D' : 7.700,
		'C' : 0.900,
		'Q' : 5.200,
		'E' : 5.700,
		'G' : 6.700,
		'H' : 2.500,
		'I' : 2.800,
		'L' : 4.800,
		'K' : 0.300,
		'M' : 1.000,
		'F' : 2.400,
		'P' : 4.800,
		'S' : 9.400,
		'T' : 7.000,
		'W' : 1.400,
		'Y' : 5.100,
		'V' : 4.500,
    }

    return getTransformation( seq, accessibleResidues )


def getComposition( seq ):

    composition = {

		'A' : 8.300,
		'R' : 5.700,
		'N' : 4.400,
		'D' : 5.300,
		'C' : 1.700,
		'Q' : 4.000,
		'E' : 6.200,
		'G' : 7.200,
		'H' : 2.200,
		'I' : 5.200,
		'L' : 9.000,
		'K' : 5.700,
		'M' : 2.400,
		'F' : 3.900,
		'P' : 5.100,
		'S' : 6.900,
		'T' : 5.800,
		'W' : 1.300,
		'Y' : 3.200,
		'V' : 6.600,
	}

    return getTransformation( seq, composition )


def getMutability( seq ):

    mutability = {

		'A' : 100.000,
		'R' : 65.000,
		'N' : 134.000,
		'D' : 106.000,
		'C' : 20.000,
		'Q' : 93.000,
		'E' : 102.000,
		'G' : 49.000,
		'H' : 66.000,
		'I' : 96.000,
		'L' : 40.000,
		'K' : 56.000,
		'M' : 94.000,
		'F' : 41.000,
		'P' : 56.000,
		'S' : 120.000,
		'T' : 97.000,
		'W' : 18.000,
		'Y' : 41.000,
		'V' : 74.000,

    }

    return getTransformation( seq, mutability )

