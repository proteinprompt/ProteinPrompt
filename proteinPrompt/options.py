#from __future__ import annotations

import os
from dataclasses import dataclass
import argparse
from pathlib import Path
from utils import makePathAbsolute


@dataclass
class SearchOptions:
    """
    Dataclass for all options necessary for searching PPIs given a or more fasta sequences
    """
    inputFile: str = ""
    modelType: str = "RF"
    threshold: float = 0.5
    outputDir: str = ""
    dbFile: str = "/model/all.proteins.HUMAN.autocorrelation.pbz2"
    modelRF: str = "/model/random_forest.pkl"
    outputFile: str = ""

    @classmethod
    def fromCmdArgs(cls, args: argparse.Namespace): # -> SearchOptions:
        """Creates ScOptions instance from cmdline arguments"""
        if args.f is not None:
            fastaFile = Path(makePathAbsolute(args.f))
            if not fastaFile.exists() and not fastaFile.is_file():
                raise ValueError("Could not find fasta input file! - " + args.f)

        if args.t > 1.0 or args.t < 0.0:
            raise ValueError("Prediction threshold of PPIs must be between 0.0 and 1.0!")

        if args.d is None:
            args.d = os.getcwd()

        return cls(
            inputFile=args.f,
            modelType=args.m,
            threshold=args.t,
            outputDir=args.d,
            dbFile=SearchOptions.dbFile,
            modelRF=SearchOptions.modelRF,
            outputFile=args.o
        )


@dataclass
class PredictOptions:
    """
    Dataclass for all options necessary for predicting given protein pairs
    """
    inputFile: str = ""
    inputCSV: str = ""
    proteinPairs: str = ""
    outputDir: str = ""
    modelType: str = "RF"
    modelRF: str = "/model/random_forest.pkl"
    outputFile: str = ""

    @classmethod
    def fromCmdArgs(cls, args: argparse.Namespace): #-> PredictOptions:
        """Creates ScOptions instance from cmdline arguments"""
        if args.f is not None:
            dbFile = Path(makePathAbsolute(args.f))
            if not dbFile.exists() and not dbFile.is_file():
                raise ValueError('Could not find fasta input file! - ' + args.f)

        if args.c is not None:
            dbFile = Path(makePathAbsolute(args.c))
            print( dbFile)
            if not dbFile.exists() and not dbFile.is_file():
                raise ValueError('Could not find feature vector input file! - ' + args.c)

        if args.f is None and args.c is None:
            raise ValueError('You have to specify either a protein fasta file or a preprocessed feature vector file!')

        if args.p is not None:
            pairFile = Path(makePathAbsolute(args.p))
            if not pairFile.exists() and not pairFile.is_file():
                raise ValueError('Could not find protein pairs input file! - ' + args.p)

        if args.d is None:
            args.d = os.getcwd()

        return cls(
            inputFile=args.f,
            inputCSV=args.c,
            proteinPairs=args.p,
            modelType=args.m,
            outputDir=args.d,
            outputFile=args.o,
            modelRF=PredictOptions.modelRF
       )


def createCommandlineParser() -> argparse.ArgumentParser:
    """
    Build the parser for arguments with its two subparsers
    """
    parser = argparse.ArgumentParser(prog='ProteinPrompt')
    subparsers = parser.add_subparsers(help="Sub programs of ProteinPrompt")

    parser_search = subparsers.add_parser("search",
                                         help="Search with a given protein sequence for PPIs in the "
                                              "ProteinPrompt database.")
    parser_search.set_defaults(method="search")
    parseInputSearch(parser_search)

    parser_predict = subparsers.add_parser("predict",
                                             help="Predict binding probabilities of given protein pairs.")
    parser_predict.set_defaults(method="predict")
    parseInputPredict(parser_predict)

    return parser


def parseInputSearch(parser: argparse.ArgumentParser) -> None:
    """
    Parse the input arguments for Search method.

    :return: A namespace object built up from attributes parsed out of the cmd line.
    """

    parser.add_argument('-f', metavar='FILE', type=str,
                        help="Fasta file containing protein sequences to search for PPIs.",
                        required=True)
    parser.add_argument('-m', metavar='STR', type=str,
                        help="Method to predict PPIs.",
                        required=False, default="RF", choices=['RF', 'NN', 'HYBRID'])
    parser.add_argument('-t', metavar='FLOAT', type=float,
                        help='Threshold for predicting PPIs',
                        required=False, default=0.5)
    parser.add_argument('-d', metavar='DIF', type=str,
                        help="Output directory. It will contain logging information and the ProteinPrompt output.",
                        required=False, default=os.getcwd())
    parser.add_argument('-o', metavar='FILE', type=str,
                        help="Output file to store the prediction scores. If not specified, "
                             "output will be printed to STDOUT.",
                        required=False)


def parseInputPredict(parser: argparse.ArgumentParser) -> None:
    """
    Parse the input arguments for Predict method.

    :return: A namespace object built up from attributes parsed out of the cmd line.
    """

    parser.add_argument('-f', metavar='FILE', type=str,
                        help="Fasta file containing protein sequences.",
                        required=False)
    parser.add_argument('-c', metavar='FILE', type=str,
                        help="Preprocessed CSV file containing the feature vectors of length 210 for each protein.",
                        required=False)
    parser.add_argument('-p', metavar='FILE', type=str,
                        help="The csv file containing the protein pairs. "
                             "All proteins have to be specified in the given fasta file."
                             "Two-column design: protein1 tab protein2",
                        required=True)
    parser.add_argument('-m', metavar='STR', type=str,
                        help="Method to predict PPIs.",
                        required=False, default="RF", choices=['RF', 'NN', 'HYBRID'])
    parser.add_argument('-d', metavar='DIR', type=str,
                        help="Output directory. It will contain logging information and the ProteinPrompt "
                             "output if specified.",
                        required=False, default=os.getcwd())
    parser.add_argument('-o', metavar='FILE', type=str,
                        help="Output file to store the prediction scores. If not specified, "
                             "output will be printed to STDOUT.",
                        required=False)
