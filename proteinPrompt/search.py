import logging

import file_functions
import utils
import options
from typing import Tuple
from file_functions import write_prediction_scores, read_compressed_pickle
from machine_learning import load_model
from numpy import array, concatenate, vstack, tile

def construct_prediction_matrix( fdata: dict, dbFile: str) -> Tuple[array, list]:

    """

    Read the input protein pairs and construct the prediction matrix of feature vectors.

    :param fdata: Dictionary of feature vectors.
    :param ppFile: CSV file containing protein pairs.
    :return: Tuple. Prediction matrix and list of protein pairs
    """

    predict = []
    proteinPairs = []

    db = read_compressed_pickle( dbFile)
    db_features = array( list( db.values()))
    db_names = list( db.keys())
    size = len( db_names)

    for key in fdata:

        # add feature vector matrix
        prot = tile(array(fdata[key]), (size, 1))
        predict.append( concatenate( (prot, db_features), axis=1))

        # add list of protein pairs
        name = [key] * size
        proteinPairs.extend( list( [a,b] for a,b in zip( name, db_names)))


    return vstack(predict), proteinPairs



def run( opts: options.SearchOptions) -> None:

    """

    The main function for scanning a given protein against a protein database.

    :param opts: Class containing command line arguments
    :return: Nothing
    """

    logging.info("Scanning protein against database for potential PPIs.")

    # When the fasta file was given, run autocorrelation to translate aa into feature vectors
    logging.info("Translating protein fasta sequences into feature vectors.")
    fdata = utils.featurize_fasta_to_dict( opts.inputFile)

    # construct prediction matrix from given protein pairs
    logging.info("Construct prediction matrix.")
    predict, proteinPairs = construct_prediction_matrix( fdata, opts.dbFile)

    # score the constructed prediction matrix
    logging.info("Search the database for PPIs with random forests.")
    model = load_model(opts.modelRF)
    scores = model.predict_proba(predict)

    # combine proteinPairs with prediction scores
    logging.info("Write prediction scores.")
    if opts.outputFile is not None:
        outfile = opts.outputDir + "/" + opts.outputFile
        write_prediction_scores(outfile, scores=scores, ppairs=proteinPairs, threshold=opts.threshold)
    else:
        utils.print_prediction_scores(scores=scores, ppairs=proteinPairs, threshold=opts.threshold)
