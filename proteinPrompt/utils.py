from pathlib import Path
from re import sub, search
from file_functions import read_fasta_file, write_feature_CSV_file
import os
import AminoAcidFeatures as F
import vector_functions as V
from collections import OrderedDict
from numpy import array

def makePathAbsolute(p: str) -> str:
    path = Path(p)
    if path.is_absolute():
        return p
    else:
        return str(path.absolute())

def createDirectory(directory: str):
    path = makePathAbsolute(Path(directory))
    if not os.path.exists(path):
        os.mkdir(path)


def features_sequences( proteins: dict, lag: int) -> dict:

    """

    Tranlsate amino acid sequences into feature vectors of length 210

    :param proteins: Dictionary of protein names and their amino acid sequences
    :param int: Length of the lag used to calculate the auto correlation
    :return: Dictionary of proteins and their feature vectors
    """

    featureVec = {}

    for key in proteins:

        if len(proteins[key]) <= lag:
            print(key + " too short: " + str(len(proteins[key])))
            continue

        if search("X", proteins[key]):
            print(key + " 'X' detected")
            continue

        if search("Z", proteins[key]):
            print(key + " 'Z' detected")
            continue

        if search("B", proteins[key]):
            print(key + " 'B' detected")
            continue

        if search("U", proteins[key]):
            print(key + " 'U' detected")
            continue

        # transform proteins[ key] vector in feature vectors
        features = OrderedDict()
        features['hydrophobicity'] = F.getHydrophobicity(proteins[key])
        features['hydrophilicity'] = F.getHydrophilicity(proteins[key])
        features['volume'] = F.getVolumeOfSideChains(proteins[key])
        features['polarity'] = F.getPolarity(proteins[key])
        features['polarizability'] = F.getPolarizability(proteins[key])
        features['surface'] = F.getSurfaceArea(proteins[key])
        features['charge'] = F.getChargeOfSideChains(proteins[key])

        # calculate autocorrelation and generate feature vector
        f = []
        for k in features:
            for i in range(1, lag + 1):
                f.append(V.calcAutoCorrelation(features[k], i))

        featureVec[key] = ['{:0.5f}'.format(i) for i in f]

    return featureVec


def featurize_fasta_to_file( fastaFile: str) -> str:

    """

    Read a given fasta file, translate the sequences into feature vectors and write
    the content to a CSV file.

    :param fastaFile: Input fasta file
    :return: Name of output file.
    """

    featureVec = featurize_fasta_to_dict( fastaFile)

    csvFile = sub( ".fa", "_autocorrelation.csv", fastaFile)
    write_feature_CSV_file( csvFile, featureVec)

    return csvFile


def featurize_fasta_to_dict( fastaFile: str) -> dict:

    """

    Read a given fasta file and translate the sequences into feature vectors.

    :param fastaFile: Input fasta file.
    :return: Dictionary of feature vectors.
    """

    proteins = read_fasta_file( fastaFile)
    featureVec = features_sequences( proteins, 30)

    return featureVec


def print_prediction_scores(scores: array, ppairs: list, threshold: float = 0.0 ) -> None:

    """
    Writes the predicted scores to STDOUT

    :param scores:  Numpy array of prediction scores
    :param ppairs:  List of protein pairs.
    :return: Nothing.
    """
    for i in range(len(ppairs)):
        if scores[i][1] >= threshold:
            print("\t".join( ppairs[i]) + "\t" + "\t".join( [ str(x) for x in scores[i]]))
