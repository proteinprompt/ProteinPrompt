# ProteinPrompt

Dockerized container of ProteinPrompt, a tool to predict protein-protein interactions based on the amino acid sequence alone.

## Availability 

The ProteinPrompt webserver is available at https://proteinformatics.uni-leipzig.de/protein_prompt/

The Docker image can be downloaded from the container registry wihtin this repository: https://gitlab.hzdr.de/proteinprompt/ProteinPrompt/container_registry/

The trained random forest model (pickle file) is uploaded to zenodo.org: https://doi.org/10.5281/zenodo.5708224

## Run

The containerized version of ProteinPrompt can be applied in two different operating modes:

1. _search_   -   Scan our human protein database for potential PPIs with a given set of proteins

2. _predict_  -   Predict given protein-protein pairs to to form PPIs

To make input files available to the docker image, you need to bind the correct directory where your input (and output) files are located, e.g., with the argument: ```-v $(pwd)/data:/data```. Then you can specify files in your ```data/``` directory to be read by the proteinprompt docker image.

By default, proteinprompt prints the results on STDOUT. For both operating modes, outputfiles can be specified with the ```-o``` option.

**Please note** that the containerized version currently only supports the random forest approach. 

### Search

You need to supply the fasta file containing the protein sequences that should be scanned against the human protein database:

```docker run  -v $(pwd)/data:/data proteinprompt search -f data/input.fa -o data/output.csv```

### Predict

To predict a set of protein pairs, you'll need to provide a tab-separated csv list of those pairs and the fasta database where the amino acid sequence of those proteins can be found: 

```docker run -v $(pwd)/data:/data proteinprompt predict -f data/input_database.fa -p data/input_pairs.csv -o data/ppi.out```


## Publication

If you are using ProteinPrompt, please cite the following paper:

ProteinPrompt: a webserver for predicting protein-protein interactions

Sebastian Canzler, David Ulbricht, Markus Fischer, Nikola Ristic, Peter W. Hildebrand, René Staritzbichler

__doi:__ https://doi.org/10.1101/2021.09.03.458859
